#!/bin/python3

import sys

bracketsOpen = {}
bracketsOpen['{'] = 1
bracketsOpen['['] = 1
bracketsOpen['('] = 1

bracketsClose = {}
bracketsClose['}'] = '{'
bracketsClose[']'] = '['
bracketsClose[')'] = '('

# Get number of test cases (first line)
t = int(input().strip())
for a0 in range(t):
    # Get next line from file (the test case itself)
    s = input().strip()
    sLen = len(s)
    stack = []
    fail = 0
    for index in range(sLen):
        char = s[index]
        # Only add opening brackets to the stack
        if char in bracketsOpen and index < sLen:
            #print("Adding %s to stack." % s[index])
            stack.append(char)
        # If it was not an opening bracket, check to see if it closes the last open bracket (balance condition)
        else:
            # If you are checking a closing bracket, make sure the stack exists:
            if len(stack)>0:
                # Pop the last opening bracket off the stack
                temp = stack.pop()
            
                # Check to see if the current bracket closes it.
                # If the bracket does close it, remove the bracket from the stack (do nothing; popping it removed it)
                # If the bracket does NOT close it, activate the fail condition.
                if temp != bracketsClose[char]:
                    fail = 1
            # If the stack was empty, fail.
            else:
                fail = 1
    # Exit for loop
    
    # Check to see if any orphans are left in the stack (unclosed brackets)
    if len(stack)>0:
        fail = 1
    
    # Check failure condition
    if fail==0:
        # All brackets were matched to end of string: valid bracket-ry
        print("YES")
    else:
        # A failure has occurred: violently reject the bracket-ry!
        print("NO")
