def minion_game(string):
    # your code goes here
    
    # ### Game Rules:
    # Both players are given the same string
    # Both players have to make substrings using the letters of the string
    # >> Stuart has to make words starting with consonants
    # >> Kevin has to make words starting with vowels
    # The game ends when both players have made all possible substrings
    
    # ### Scoring
    # A player gets +1 point for each occurrence of the substring in the string
    
    # ### Output
    # Your task is to determine the winner of the game and their score
    
    # ### Prints
    # string: the winner's name and score, separated by a space on one line,
    # or Draw if there is no winner
    
    # print("String length: {}".format(len(string)))
    
    scoreStuart = 0
    scoreKevin = 0
        
    vowelStash = { "A":1, "E":1, "I":1, "O":1, "U":1 }
    
    seenWordStash = {}
       
    # Generate a sliding scan window.
    # The width of the window will range from 1 to the length of the string + 1
    # This is to fix slice indexing.
    for window in range(1, 1+len(string)):
        # print("win: " + str(window))
        
        # Generate a sliding range across the string,
        # subtracting the window length to compensate.
        # Add 1 to fix offset/indexing.
        for slide in range(len(string) - window + 1):
            # print("slide: " + str(slide))
            
            # Slice the string using the slide and window.
            stringBit = string[slide:slide + window]
            
            # print(stringBit)
            # print("Sliced {} from {} to {}".format(stringBit, slide, slide+window))
            
            # Pass this loop if we have seen the stringBit before.
            if stringBit in seenWordStash.keys():
                # print("Seen {} already.".format(stringBit))
                continue
            
            # Otherwise add it to the stash for recognition.
            else:
                seenWordStash[stringBit] = 1
            
            score = string.count(stringBit)
            
            if stringBit[0] in vowelStash.keys():
                scoreKevin += score
                # print("Giving {} points to Kevin for {}. ({})".format(score, stringBit, scoreKevin))
            else:
                scoreStuart += score
                # print("Giving {} points to Stuart for {}. ({})".format(score, stringBit, scoreStuart))
    
    # End of big for loop
    
    if scoreKevin > scoreStuart:
        # Kevin has to make words starting with vowels
        print("Kevin " + str(scoreKevin))
    elif scoreKevin < scoreStuart:
        print("Stuart " + str(scoreStuart))
    else:
        print("Draw")

if __name__ == '__main__':
    s = input()
    minion_game(s)