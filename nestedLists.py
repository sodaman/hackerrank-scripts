
if __name__ == '__main__':
    
    classScoreList = []
    classSize = int(input())
    
    for _ in range(classSize):
        name = input()
        score = float(input())
        
        classScoreList.append([name, score])
        
    # print(classScoreList)
    
    # Use a lambda function to sort the list with two keys:
    # 1) By score, ascending
    # 2) By name, alphabetical
    # The lowest score will be index 0.
    classScoreList.sort(key=lambda student: (student[1], student[0]),
                        reverse=False)
    
    # print(classScoreList)
    
    lowestScore = classScoreList[0][1]
    
    secondLowest = 0
    
    # Iterate through the list, printing the 2nd lowest scoring names.
    for k in range(classSize):
        # Skip the early lowest scores.
        if classScoreList[k][1] == lowestScore:
            continue
        
        # Find the 2nd lowest score.
        secondLowest = classScoreList[k][1]
        
        # Print the remaining scores, as long as they are the second lowest.
        for j in range (k, classSize):
            if classScoreList[j][1] == secondLowest:
                print(classScoreList[j][0])
                
            # Break out of printing as soon as the score is too high.
            else:
                break
        
        break
    
